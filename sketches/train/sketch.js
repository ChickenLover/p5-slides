class Chelik {
  constructor(x, y, w, h, disp) {
    this.x = x
    this.y = y
    this.w = w
    this.h = h
    this.disp = disp
    this.origY = y

    this.t = 42

    this.top = createVector(0, -this.h/6)
    this.bottom = createVector(0, this.h/6)
    this.legLeft = createVector(-this.w/2, this.h/2)
    this.legRight = createVector(this.w/2, this.h/2)
    this.armLeft = createVector(-this.w/2, 0)
    this.armRight = createVector(this.w/2, 0)
  }

  display() {
    this.y = this.origY + sin(this.t + this.x) * 10
    this.t += 0.1
    push()
    translate(this.x, this.y)
    strokeWeight(2)
    line(this.legLeft.x, this.legLeft.y, this.bottom.x, this.bottom.y)
    line(this.legRight.x, this.legRight.y, this.bottom.x, this.bottom.y)
    line(this.bottom.x, this.bottom.y, this.top.x, this.top.y)
    line(this.armLeft.x, this.armLeft.y, this.top.x, this.top.y)
    line(this.armRight.x, this.armRight.y, this.top.x, this.top.y)
    strokeWeight(5)
    point(this.armLeft.x, this.armLeft.y)
    point(this.armRight.x, this.armRight.y)
    strokeWeight(2)

    line(this.top.x, this.top.y, 0, -this.h/4)
    translate(-this.w/2, -this.h/2)
    scale(this.w / 400, this.w / 400)
    this.disp()
    pop()
  }
}

let functions = [dispDefault, dima, masha, vikulya, vasya, vanya, vasya2, ayuna];
let chelicks = []

function setup() {
  createCanvas(windowWidth, windowHeight);
  rectMode(CENTER);

  for (let i = 0; i < functions.length; i++) {
    let x = 100 * int((i + 1) / 2)
    console.log(x)
    if (i % 2 == 1) {
      x = -x
    }
    chelicks.push(new Chelik(width/2 + x, height - 150, 100, 300, functions[i]))
  }
}

function draw() {
  background("#F3F3EC");
  for (let i = 0; i < chelicks.length; i++) {
    chelicks[i].display()
  }
}

function dispDefault() {
  scale(0.5, 0.5)
  let happy = true;
  fill("white")
  ellipse(400, 400, 600, 600)
  
  fill("blue")
  ellipse(250, 250, 75, 75)
  ellipse(550, 250, 75, 75)
 
  fill("grey")
  if (!happy) {
    beginShape()
    vertex(350, 525)
    vertex(450, 525)
    vertex(500, 575)
    vertex(300, 575)
    endShape(CLOSE)
  } else {
    beginShape()
    vertex(300, 525)
    vertex(500, 525)
    vertex(450, 575)
    vertex(350, 575)
    endShape(CLOSE)
  }
}

function dima() {
  scale(1.5, 1.5)
  translate(-70, 0)
  fill(255, 192, 203);
  // ears
  ellipse(165, 150, 25, 50);
  ellipse(235, 150, 25, 50);
  
  // face
  circle(200, 200, 125);
  
  // left eye
  fill(255);
  circle(175, 170, 25);
  fill(0);
  circle(175, 170, 10);
  
  // right eye
  fill(255);
  circle(225, 170, 25);
  fill(0);
  circle(225, 170, 10);
  
  // nose
  fill(255, 192, 203);
  ellipse(200, 210, 50, 25);
  fill(0);
  circle(190, 210, 10);
  circle(210, 210, 10);
  
  // mouth
  noFill();
  arc(200, 225, 50, 50, PI * .25, PI * .75);
}

function masha() {
  stroke("purple");
  strokeWeight(5);
  
  fill("orange");
  
  // noStroke()
  // noFill()
  
  ellipse(200, 200, 200);
  
  fill("purple")
  ellipse(160, 170, 30);
  ellipse(240, 170, 30);
  
  
  ellipse(67, 67, 20, 20);
  ellipse(83, 67, 20, 20);
  triangle(91, 73, 75, 95, 59, 73);
  
  arc(200, 220, 30, 90, 0, PI+QUARTER_PI);
}

function vikulya() {
  stroke("black");
  strokeWeight(2);
  
  fill("wheat")
  
  //nostroke()
  //nofill()
  
  ellipse(200,200,180);
  
  fill("red");
  ellipse(165,180,15);
  ellipse(225,180,15);
  line(175,250,225,230);
  line(200,110,215,80);
  line(200,110,185,80);
  line(200,110,200,80);

}

function vasya() {
  let pirate = 1
  let x = 400;
  let y = 400;
  let eyeCol = "white"
  // background("blue");
  stroke("yellow");
  strokeWeight(5);
  fill("orange");
  //line(0, 0, width, height);
  
  ellipse(x/2, y/2, x/2);
  // eyes
  // left
  ellipse(x/2-x/20*2, y/2-50, x/20);
  // right
  ellipse(x/2+x/20*2, y/2-50, x/20);
  // nose
  triangle(x/2, y/2-y/50, x/2-x/50, y/2+y/20, x/2+x/50, y/2+y/20);
  fill("red");
  textSize(x/2);
  text("~", x/3, y/2+y/4+y/25);
  
  text("")
  // pirate
  if(pirate == 1) {
    fill("black");
    stroke("black");
    ellipse(x/2+x/20*2,y/2-50, x/8);
    strokeWeight(10);
    line(x/2, y/4, x/2+x/20*2, y/2-50);
    line(x/4*3, y/2, x/2+x/20*2, y/2-50);
  }
}

function vanya() {
  translate(0, 120)
  
  noFill();
  
  ellipse(200, 100, 170);
  ellipse(160, 90, 20);
  fill("red");
  ellipse(240, 90, 20);
  line(180, 120, 220, 120);
  noStroke();
  arc(210, 120.5, 20, 50, 0, HALF_PI);

  eyeX = map(mouseX, 0, width, 230, 250);
  eyeY = map(mouseY, 0, height, 80, 100);
  fill("black");
  ellipse(eyeX, eyeY, 6);
}

function vasya2() {
  translate(30, 100)
  stroke("yellow");
  let x = 400;
  let y = 400;

  textSize(x/2);
  //noFill();
    
  text("☻", x/4, y/2);
}

function ayuna() {
  stroke ("black");
  strokeWeight (0.5);
  
  //face
  fill ("pink");
  ellipse (200, 200, 200);
  
  noStroke ();
  fill ("green");
  ellipse (150, 180, 20);
  ellipse (250, 180, 20);
  
  stroke ("black");
  line (180, 240, 220, 240);
  
  noStroke ();
  fill ("yellow");
  quad(150, 100, 200, 60, 250, 100, 200, 150);
}