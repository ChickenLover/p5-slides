let pointsPerLine = 10;
let points;

function setup() {
  createCanvas(windowWidth, windowHeight);
  strokeWeight(0.1);

  init()
}

function init() {
  let pointsOffset = width / pointsPerLine;
  points = [];
  for (let i = 0; i < pointsPerLine + 1; i++) {
    let p = {
      x: i * pointsOffset,
      y: 0
    };
    points.push(p);
  }
}

function drawLine() {
  noFill()
  beginShape();
  for (let i = 0; i < points.length; i++) {
    points[i].y += random(-3, 3)
    curveVertex(points[i].x, points[i].y);
  }
  endShape();
}

function draw() {
  background(255, 5);
  push();
  translate(0, height / 2);
  drawLine();
  pop();
}

function mouseClicked() {
  background("white");
  init()
}