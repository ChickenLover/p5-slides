let total = 80; // CHANGE ME
let factor = 16;
let alphaStart = 255;
let alphaStop = 150;

let strokeStart = 32;
let strokeStop = 16;

let colorStart = 20;
let colorStop = 80;

let factorSpeed = 0.0005;
let rotationSpeed = 0.002;

let step = 30;

let factorSliderVal = 0;
let rotation = 0;
let r;
let lines = Array();
let yoff = 0.0;

let speed = 0.75
let stars = []

let logoScale = 0.20

class Star {
  constructor(x, y) {
    this.x = x
    this.y = y
    this.z = random(windowWidth);
  }

  update() {
    this.z -= speed;
    if (this.z < 1) {
      this.z = windowWidth
      this.x = random(-windowWidth / 2, windowWidth / 2);
      this.y = random(-windowHeight / 2, windowHeight / 2)
    }
  }

  display() {
    push()

    let colour = map(this.z, 0, windowWidth, 0, 255)
    fill(colour)
    noStroke()

    let sx = map(this.x / this.z, -1, 1, -width/2, width/2);
    let sy = map(this.y / this.z, -1, 1, -height/2, height/2);

    if ((sx * sx + sy * sy) < r * r) {
      pop()
      return
    }
    let rad = map(this.z, 0, windowWidth, 8, 0);

    ellipse(sx, sy, rad, rad)

    pop()
  }
}

function setup() {
  let x = min(windowHeight, windowWidth)
  createCanvas(windowWidth, windowHeight);
  r = x / 2 - x / 11;
  // r = x;

  for(let i = 0; i < 1000; i++) {
    stars.push(new Star(random(-windowWidth, windowWidth), random(-windowHeight, windowHeight)))
  }
}

function getVector(index, total) {
  const angle = map(index % total, 0, total, 0, TWO_PI);
  const v = p5.Vector.fromAngle(angle + PI);
  v.mult(r);
  return v;
}


function draw() {
  randomSeed(millis())
  background("white");
  translate(windowWidth / 2, windowHeight / 2);

  /*
  for (let i = 0; i < stars.length; i++) {
    stars[i].update()
    stars[i].display()
  }

  push()
  scale(logoScale, logoScale)
  strokeCap(ROUND);
  strokeJoin(ROUND);
  noStroke()
  drawA()
  drawB()
  drawSecondA()
  pop()
  */

  factor += factorSpeed;
  rotation += rotationSpeed;

  push()
  rotate(rotation);
  stroke(255, 150);

  noFill()
  stroke("black");
  ellipse(0, 0, r*2, r*2)

  strokeWeight(1);
  let al, bl;
  for (let i = 0; i < total; i++) {
    randomSeed(i);
    let alpha = int(map(i, 0, total, alphaStop, alphaStart));

    let rc = random(colorStart, colorStop);
    let gc = random(colorStart, colorStop);
    let bc = random(colorStart, colorStop);
    stroke(0, 0, 0, alpha);

    let s = int(map(i, total, 0, strokeStop, strokeStart));
    const a = getVector(i, total);
    const b = getVector(i * factor, total);
    const m = createVector((a.x + b.x) / 2, (a.y + b.y) / 2);

    let off = 20
    let step = 10
    
    /*for (let j = -off; j < off; j+=step) {
      if (abs(a.x - b.x) < abs(a.y - b.y)) {
        line(a.x - j, a.y, b.x - j, b.y);
      } else {
        line(a.x, a.y - j, b.x, b.y - j);
      }
    }*/
  
    line(a.x, a.y, b.x, b.y);
    //noFill();
    //bezier(a.x, a.y, c1.x, c1.y, c2.x, c2.y, b.x, b.y);
  }
  pop()

  /*
  push()
  scale(logoScale, logoScale)
  strokeCap(ROUND);
  strokeJoin(ROUND);
  noStroke()
  drawK()
  drawI()
  drawHira()
  pop()
  */
}

function drawA() {
  fill("#bc2739")
  beginShape();
  vertex(-309.237, -140.0);
  vertex(-250.432, -140.0);
  vertex(-205.441, 140.0);
  vertex(-248.854, 140.0);
  vertex(-256.747, 84.4);
  vertex(-256.747, 85.2);
  vertex(-306.079, 85.2);
  vertex(-313.973, 140.0);
  vertex(-354.228, 140.0);
  vertex(-309.237, -140.0);
  endShape();
  fill("#F3F3EC")
  beginShape()
  vertex(-261.877, 47.2);
  vertex(-281.216, -91.2);
  vertex(-282.005, -91.2);
  vertex(-300.949, 47.2);
  vertex(-261.877, 47.2);
  endShape();
}

function drawK() {
  fill("#bc2739")
  beginShape();
  vertex(-184.379, -140.0);
  vertex(-140.966, -140.0);
  vertex(-140.966, -22.0);
  vertex(-85.714, -140.0);
  vertex(-42.301, -140.0);
  vertex(-94.002, -37.2);
  vertex(-41.512, 140.0);
  vertex(-86.898, 140.0);
  vertex(-123.601, 15.2);
  vertex(-140.966, 50.8);
  vertex(-140.966, 140.0);
  vertex(-184.379, 140.0);
  vertex(-184.379, -140.0);
  endShape();
}

function drawI() {
  fill("#bc2739")
  beginShape();
  vertex(-20.965, -140.0);
  vertex(22.448, -140.0);
  vertex(22.448, 140.0);
  vertex(-20.965, 140.0);
  vertex(-20.965, -140.0);
  endShape();
}

function drawB() {
  fill("#bc2739")
  beginShape();
  vertex(54.961, -140.0);
  vertex(120.475, -140.0);
  bezierVertex(142.839, -140.0,159.152, -134.667,169.413, -124.0);
  bezierVertex(179.674, -113.6,184.805, -97.467,184.805, -75.6);
  vertex(184.805, -64.4);
  bezierVertex(184.805, -50.0,182.437, -38.267,177.701, -29.2);
  bezierVertex(173.228, -20.133,166.255, -13.6,156.784, -9.6);
  vertex(156.784, -8.8);
  bezierVertex(178.358, -1.333,189.146, 18.133,189.146, 49.6);
  vertex(189.146, 73.6);
  bezierVertex(189.146, 95.2,183.489, 111.733,172.175, 123.2);
  bezierVertex(161.125, 134.4,144.812, 140.0,123.237, 140.0);
  vertex(54.961, 140.0);
  vertex(54.961, -140.0);
  endShape();
  fill("#F3F3EC")
  beginShape()
  vertex(115.344, -26.0);
  bezierVertex(124.027, -26.0,130.473, -28.267,134.683, -32.8);
  bezierVertex(139.155, -37.333,141.392, -44.933,141.392, -55.6);
  vertex(141.392, -71.2);
  bezierVertex(141.392, -81.333,139.55, -88.667,135.867, -93.2);
  bezierVertex(132.446, -97.733,126.921, -100.0,119.291, -100.0);
  vertex(98.374, -100.0);
  vertex(98.374, -26.0);
  vertex(115.344, -26.0);
  endShape();
  fill("#F3F3EC")
  beginShape()
  vertex(123.237, 100.0);
  bezierVertex(130.868, 100.0,136.524, 98.0,140.208, 94.0);
  bezierVertex(143.891, 89.733,145.733, 82.533,145.733, 72.4);
  vertex(145.733, 48.0);
  bezierVertex(145.733, 35.2,143.497, 26.4,139.024, 21.6);
  bezierVertex(134.814, 16.533,127.71, 14.0,117.712, 14.0);
  vertex(98.374, 14.0);
  vertex(98.374, 100.0);
  vertex(123.237, 100.0);
  endShape();
}

function drawSecondA() {
  fill("#bc2739")
  beginShape();
  vertex(248.067, -140.0);
  vertex(306.872, -140.0);
  vertex(351.863, 140.0);
  vertex(308.451, 140.0);
  vertex(300.557, 84.4);
  vertex(300.557, 85.2);
  vertex(251.225, 85.2);
  vertex(243.331, 140.0);
  vertex(203.076, 140.0);
  vertex(248.067, -140.0);
  endShape();
  fill("#F3F3EC")
  beginShape()
  vertex(295.427, 47.2);
  vertex(276.088, -91.2);
  vertex(275.299, -91.2);
  vertex(256.355, 47.2);
  vertex(295.427, 47.2);
  endShape();
}

function drawHira() {
  push()
  fill("#202023")
  beginShape();
  vertex(-386.524, -52.803);
  bezierVertex(-391.597, -52.803,-395.271, -54.475,-397.568, -57.818);
  bezierVertex(-401.388, -63.899,-402.035, -74.973,-397.192, -79.424);
  bezierVertex(-391.43, -84.719,-372.642, -84.627,-356.838, -86.361);
  bezierVertex(-339.3, -88.286,-313.666, -89.621,-295.399, -90.896);
  bezierVertex(-270.264, -92.212,-259.471, -92.547,-235.004, -92.923);
  bezierVertex(-225.109, -92.923,-218.512, -91.126,-215.213, -87.511);
  bezierVertex(-211.664, -84.167,-209.89, -80.302,-209.89, -75.935);
  bezierVertex(-209.89, -70.523,-211.664, -62.812,-215.213, -52.782);
  bezierVertex(-222.374, -33.245,-233.25, -10.803,-243.0, 7.794);
  bezierVertex(-247.822, 17.05,-251.496, 23.862,-254.043, 28.25);
  bezierVertex(-254.795, 29.546,-255.943, 30.445,-257.467, 30.946);
  bezierVertex(-266.047, 34.185,-281.704, 26.433,-281.454, 16.277);
  bezierVertex(-281.454, 15.504,-279.93, 12.286,-276.882, 6.624);
  bezierVertex(-265.901, -14.355,-251.079, -41.896,-242.624, -64.379);
  bezierVertex(-252.77, -64.379,-264.064, -63.857,-276.506, -62.833);
  bezierVertex(-311.85, -60.681,-351.723, -55.457,-386.524, -52.803);
  endShape();
  beginShape()
  vertex(-358.341, -1.129);
  bezierVertex(-363.289, -11.848,-352.037, -18.054,-343.122, -18.096);
  bezierVertex(-335.335, -18.096,-331.703, -11.952,-329.031, -5.37);
  bezierVertex(-321.954, 20.039,-327.465, 48.456,-341.223, 71.024);
  bezierVertex(-346.045, 79.006,-352.016, 86.068,-359.114, 92.253);
  bezierVertex(-367.381, 99.191,-377.902, 105.647,-388.8, 107.298);
  bezierVertex(-396.9, 107.486,-403.142, 99.901,-402.891, 91.856);
  bezierVertex(-402.891, 85.943,-399.467, 81.701,-392.62, 79.131);
  bezierVertex(-387.798, 77.334,-383.1, 74.367,-378.529, 70.25);
  bezierVertex(-361.097, 54.119,-348.801, 21.856,-358.341, -1.129);
  endShape();

  fill("#202023")
  beginShape();
  vertex(-107.596, -56.523);
  bezierVertex(-116.594, -56.209,-120.456, -63.878,-120.539, -71.567);
  bezierVertex(-120.477, -79.926,-117.262, -83.436,-109.12, -85.066);
  vertex(-72.023, -88.806);
  bezierVertex(-72.023, -88.806,-73.296, -93.263,-74.111, -96.119);
  bezierVertex(-75.587, -101.293,-78.14, -105.376,-77.889, -109.367);
  bezierVertex(-78.307, -117.266,-65.676, -123.388,-59.247, -123.284);
  bezierVertex(-47.535, -122.991,-40.395, -98.376,-38.621, -89.329);
  bezierVertex(-31.398, -89.433,-1.723, -89.851,21.92, -89.851);
  bezierVertex(32.358, -89.851,34.53, -82.893,34.446, -76.269);
  bezierVertex(34.446, -61.642,0.0, -59.552,-20.062, -58.069);
  bezierVertex(-13.569, -38.448,-11.58, -29.333,-8.455, -10.448);
  bezierVertex(-5.139, 9.581,-4.071, 26.328,-4.071, 41.352);
  bezierVertex(7.683, 40.098,32.715, 38.903,56.074, 35.564);
  bezierVertex(63.673, 34.478,70.937, 33.62,75.865, 39.806);
  bezierVertex(79.271, 44.081,78.913, 49.355,78.913, 53.471);
  bezierVertex(78.913, 56.292,78.286, 56.794,77.013, 59.092);
  bezierVertex(75.74, 61.14,73.84, 62.436,71.293, 62.958);
  bezierVertex(62.901, 64.546,52.776, 65.737,42.358, 66.427);
  bezierVertex(28.643, 67.659,12.672, 69.101,-1.795, 69.895);
  bezierVertex(-1.795, 77.104,-1.92, 84.042,-2.171, 90.728);
  bezierVertex(-2.442, 101.761,-3.256, 112.376,-9.394, 120.442);
  bezierVertex(-13.59, 126.459,-24.613, 126.355,-30.333, 121.591);
  bezierVertex(-33.381, 119.543,-34.905, 116.701,-34.905, 113.107);
  bezierVertex(-34.905, 109.513,-34.654, 104.101,-34.132, 96.892);
  bezierVertex(-33.381, 89.433,-32.984, 81.45,-32.984, 72.967);
  bezierVertex(-44.654, 73.74,-56.324, 74.639,-68.015, 75.662);
  bezierVertex(-79.434, 76.436,-89.976, 77.209,-99.621, 77.982);
  bezierVertex(-113.149, 79.048,-127.867, 80.364,-138.075, 80.677);
  bezierVertex(-150.351, 81.346,-157.783, 64.253,-149.495, 55.979);
  bezierVertex(-147.219, 53.931,-142.125, 52.259,-134.276, 50.964);
  bezierVertex(-126.155, 49.418,-114.235, 48.143,-98.494, 47.098);
  bezierVertex(-82.753, 46.074,-61.439, 44.653,-34.529, 42.856);
  bezierVertex(-36.053, 28.71,-35.865, 12.161,-40.625, -8.17);
  bezierVertex(-45.259, -28.021,-51.147, -41.624,-58.119, -57.338);
  bezierVertex(-74.465, -57.505,-93.964, -56.564,-107.596, -56.523);
  endShape();

  fill("#202023")
  beginShape();
  vertex(259.242, -20.708);
  bezierVertex(250.808, -24.03,251.372, -34.311,256.57, -40.391);
  bezierVertex(260.724, -46.158,268.699, -46.388,275.233, -43.86);
  bezierVertex(284.628, -39.221,297.801, -29.129,306.443, -21.941);
  bezierVertex(324.794, -7.314,339.282, 11.952,349.094, 34.018);
  bezierVertex(354.417, 45.343,358.363, 57.546,360.889, 70.668);
  vertex(360.889, 72.591);
  bezierVertex(360.889, 77.23,359.114, 80.824,355.565, 83.394);
  bezierVertex(348.655, 88.304,336.589, 88.931,335.002, 79.152);
  bezierVertex(331.954, 65.256,327.758, 53.179,322.435, 42.877);
  bezierVertex(312.915, 23.842,299.972, 7.898,283.98, -3.803);
  bezierVertex(276.883, -8.944,267.238, -17.114,259.242, -20.708);
  endShape();
  beginShape()
  vertex(178.68, -11.117);
  bezierVertex(178.367, -18.764,185.549, -22.254,192.02, -22.317);
  bezierVertex(197.573, -22.317,205.799, -16.132,205.339, -9.591);
  bezierVertex(205.339, -3.929,204.317, 2.361,202.292, 9.319);
  bezierVertex(197.281, 29.504,176.238, 63.25,153.566, 65.277);
  bezierVertex(142.293, 66.322,135.216, 50.734,144.422, 44.445);
  bezierVertex(163.629, 32.889,176.134, 10.406,178.68, -11.117);
  endShape();
  beginShape()
  vertex(315.963, -55.875);
  bezierVertex(302.352, -60.158,310.097, -79.738,322.059, -79.403);
  bezierVertex(329.324, -79.152,336.526, -74.89,342.622, -69.75);
  bezierVertex(346.421, -66.929,349.991, -63.314,353.29, -58.947);
  bezierVertex(357.569, -53.493,363.352, -46.66,363.561, -39.263);
  bezierVertex(363.77, -32.326,356.233, -25.973,349.469, -26.141);
  bezierVertex(345.67, -26.141,342.872, -27.561,341.098, -30.382);
  bezierVertex(337.549, -36.547,333.729, -41.582,329.679, -45.427);
  bezierVertex(325.879, -49.293,321.307, -52.761,315.963, -55.854);
  vertex(315.963, -55.875);
  endShape();
  beginShape()
  vertex(346.421, -82.12);
  bezierVertex(333.353, -87.135,337.466, -107.947,351.369, -107.591);
  bezierVertex(363.853, -106.714,374.98, -96.83,382.976, -87.135);
  bezierVertex(387.109, -81.597,392.412, -74.618,392.495, -67.451);
  bezierVertex(392.767, -59.364,385.752, -53.994,378.404, -53.952);
  bezierVertex(374.604, -53.952,371.807, -55.373,370.033, -58.194);
  bezierVertex(363.248, -69.687,357.006, -75.517,346.421, -82.12);
  endShape();
  pop()
}

function drawAkiba() {
  push()
  scale(0.75, 0.75)
  strokeCap(ROUND);
  strokeJoin(ROUND);
  noStroke()

  drawA()
  drawK()
  drawI()
  drawB()
  drawSecondA()
  drawHira()
}
