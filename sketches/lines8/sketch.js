let lines = 15;
let pointsPerLine = 20;
let yDiff = 5;

function setup() {
  createCanvas(windowWidth, windowHeight);
  noLoop()
}

function drawLine(n) {
  let pointsOffset = width / pointsPerLine;
  let points = [];
  for (let i = 1; i < pointsPerLine; i++) {
    let p = {
      x: i * pointsOffset,
      y: random(-i*n/12, i*n/12)
    };
    points.push(p);
  }

  for (let i = 0; i < points.length - 1; i++) {
    line(points[i].x, points[i].y, points[i+1].x, points[i+1].y);
  }
}

function draw() {
  let lineOffset = height / lines;
  background("white");
  for (let y = 1; y < lines; y++) {
    push()
    translate(0, y * lineOffset)
    drawLine(y)
    pop()
  }
}

function mouseClicked() {
  redraw()
}