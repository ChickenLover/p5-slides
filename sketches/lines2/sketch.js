function setup() {
  createCanvas(windowWidth, windowHeight);
  noLoop()
}

function draw() {
  background("white")
  if (random() > 0.5) {
    line(0, 0, width, height);
  } else {
    line(0, height, width, 0);
  }
}

function mouseClicked() {
  redraw()
}
