function setup() {
  createCanvas(windowWidth, windowHeight);
  rectMode(CENTER);
}

function recDraw(w) {
  if (w < 50) {
    return;
  }
  rect(0, 0, w, w);

  // Call self
  recDraw(w * 3 / 4);
}

function draw() {
  translate(width/2, height/2);
  recDraw(width);
}

function mouseClicked() {
  redraw()
}
