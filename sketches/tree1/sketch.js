function setup() {
  createCanvas(windowWidth, windowHeight);
  noLoop()
  angleMode(DEGREES);
}

let lenMax = 60;
let lenMin = 10;
let lenMul = 0.1;

function draw() {
  push()
  background("white")
  translate(width/2, height)
  branch(lenMax);
  pop()
}

function branch(len) {
  push()
  if (len > lenMin) {
    strokeWeight(map(len, lenMin, lenMax, 1, 5))
    line(0, 0, 0, -len)
    translate(0, -len)
    push()
    rotate(random(-10, -20) - len * lenMul)
    branch(len * random(0.6, 0.9))
    pop()
    rotate(random(10, 20) + len * lenMul)
    branch(len * random(0.6, 0.9))
  }
  pop()
}

function mouseClicked() {
  redraw()
}
