let tiles = 1

function setup() {
  createCanvas(windowWidth, windowHeight);
  noLoop()
}

function drawLine(w, h) {
  if (random() > 0.5) {
    line(0, 0, w, h);
  } else {
    line(0, h, w, 0);
  }
}

function draw() {
  background("white");
  let tileWidth = width / tiles;
  let tileHeight = height / tiles;

  for (let x = 0; x < tiles; x++) {
    for (let y = 0; y < tiles; y++) {
      push()
      translate(x * tileWidth, y * tileHeight);
      drawLine(tileWidth, tileHeight)
      pop()
    }
  }
}

function mouseClicked() {
  tiles *= 2
  redraw()
}
