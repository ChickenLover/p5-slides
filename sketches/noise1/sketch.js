DEBUG = false;

class Glass {
  
  constructor(x, y, w, h, color) {    
    this.yoff = x;

    this.x = x
    this.y = y
    this.w = w / 2
    this.h = h / 2
    
    this.p1 = createVector(-this.w, -this.h);
    this.p4 = createVector(this.w, -this.h);

    this.p2 = createVector(-this.w, this.h);
    this.p3 = createVector(this.w, this.h);
    
    this.color = color;
  }
  
  display() {
    push()
    translate(this.x, this.y)
    
    stroke("red")
    strokeWeight(5)
    
    this.displayLiquid()
    this.displayGlass()
    
    pop()
  }
  
  displayGlass() {
    stroke("black")
    strokeWeight(3)
    noFill();
    bezier(
        this.p1.x, this.p1.y,
        this.p2.x, this.p2.y,
        this.p3.x, this.p3.y,
        this.p4.x, this.p4.y
    );
    
    bezier(
        this.p1.x, this.p1.y,
        this.p2.x, -this.h / 7 * 6,
        this.p3.x, -this.h / 7 * 6,
        this.p4.x, this.p4.y,
    );
    
    bezier(
        this.p1.x, this.p1.y,
        this.p2.x, -this.h / 7 * 8,
        this.p3.x, -this.h / 7 * 8,
        this.p4.x, this.p4.y,
    );
    
    let base = this.glassPoint(0.5)
    let glassHandleHeight = this.h
    line(base.x, base.y, base.x, base.y + glassHandleHeight)
    
    let glassHeight = abs(this.glassPoint(0).y - base.y)
    bezier(
        this.p1.x, this.p1.y + glassHeight + glassHandleHeight,
        this.p2.x, -this.h / 7 * 6 + glassHeight + glassHandleHeight,
        this.p3.x, -this.h / 7 * 6 + glassHeight + glassHandleHeight,
        this.p4.x, this.p4.y + glassHeight + glassHandleHeight,
    );
    
    bezier(
        this.p1.x, this.p1.y  + glassHeight + glassHandleHeight,
        this.p2.x, -this.h / 7 * 8  + glassHeight + glassHandleHeight,
        this.p3.x, -this.h / 7 * 8  + glassHeight + glassHandleHeight,
        this.p4.x, this.p4.y  + glassHeight + glassHandleHeight,
    );
  }
  
  displayLiquid() {
    let t = 0.1
    
    let lp = this.glassPoint(t)
    let rp = this.glassPoint(1 - t)

    fill(this.color);
    stroke("black")
    strokeWeight(1)
    beginShape()
    
    vertex(this.p1.x, this.p1.y);
    bezierVertex(
      this.p2.x, this.p2.y,
      this.p3.x, this.p3.y,
      this.p4.x, this.p4.y
    )
    
    let xoff = 0
    for (let x = rp.x; x >= lp.x; x -= 5) {
      let y = map(noise(xoff, this.yoff), 0, 1, lp.y, lp.y - 30);
      vertex(x, y);
      if (DEBUG) {
        push()
        line(x, lp.y - 30, x, lp.y)
        strokeWeight(5)
        stroke("black")
        point(x, y)
        pop()
      }
      xoff += 0.05;
    }
    vertex(lp.x, map(noise(xoff, this.yoff), 0, 1, lp.y, lp.y - 30));
    this.yoff += 0.01;
    
    endShape(CLOSE);
  }
  
  glassPoint(t) {
    return createVector(
      bezierPoint(this.p1.x, this.p2.x, this.p3.x, this.p4.x, t),
      bezierPoint(this.p1.y, this.p2.y, this.p3.y, this.p4.y, t)
    )
  }
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  glass = new Glass(width/2, height / 2, width/4, height/2, "#BC2739E6")
}

function draw() {
  push()
  background("white")
  if (DEBUG) {
    translate(-width/2, 0)
    scale(2, 2)
  }
  glass.display()
  pop()
}

function mouseClicked() {
  DEBUG = !DEBUG;
}