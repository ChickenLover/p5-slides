function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);
  slider = createSlider(10, 30, 10);
  slider.position(10, 10);
  lenMin = 39 - slider.value();
}

let lenMax = 60;
let lenMul = 0.1;

function draw() {
  let newLenMin = 40 - slider.value();
  if (newLenMin != lenMin) {
    background("white");
    push()
    translate(width / 2, height)
    branch(lenMax)
    pop()
  }
  lenMin = 40 - slider.value();
}

function branch(len) {
  push()
  if (len > lenMin) {
    strokeWeight(map(len, lenMin, lenMax, 1, 5))
    line(0, 0, 0, -len)
    translate(0, -len)
    push()
    rotate(random(-10, -20) - len * lenMul)
    branch(len * random(0.6, 0.9))
    pop()
    rotate(random(10, 20) + len * lenMul)
    branch(len * random(0.6, 0.9))
  }
  pop()
}

function mouseClicked() {
  redraw()
}
