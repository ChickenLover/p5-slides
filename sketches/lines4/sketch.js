let tiles = 1

function setup() {
  createCanvas(windowWidth, windowHeight);
  noLoop()
}

function drawLine(w, h) {
  if (random() > 0.5) {
    line(0, h/2, w, h/2);
  } else {
    line(w/2, 0, w/2, h);
  }
}

function draw() {
  background("white");
  let tileWidth = width / tiles;
  let tileHeight = height / tiles;

  for (let x = 0; x < tiles; x++) {
    for (let y = 0; y < tiles; y++) {
      push()
      translate(x * tileWidth, y * tileHeight);
      drawLine(tileWidth, tileHeight)
      pop()
    }
  }
}

function mouseClicked() {
  tiles *= 2
  redraw()
}