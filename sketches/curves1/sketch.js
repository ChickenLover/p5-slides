let tiles = 1;

function setup() {
  createCanvas(windowWidth, windowHeight);
  noLoop()
}

function drawCurve(w, h) {
  if (random() > 0.5) {
    bezier(0, h/2, w/4, h/2, w/2, h * 3 / 4, w/2, h);
    bezier(w/2, 0, w/2, h/4, w * 3 / 4, h/2, w, h/2);
  } else {
    bezier(w/2, h, w / 2, h * 3 /4, w * 3 / 4, h / 2, w, h/2);
    bezier(0, h/2, w/4, h/2, w / 2, h / 4, w / 2, 0);
  }
}

function draw() {
  background("white");
  let tileWidth = width / tiles;
  let tileHeight = height / tiles;

  for (let x = 0; x < tiles; x++) {
    for (let y = 0; y < tiles; y++) {
      push()
      translate(x * tileWidth, y * tileHeight);
      drawCurve(tileWidth, tileHeight)
      pop()
    }
  }
}

function mouseClicked() {
  tiles *= 2
  redraw()
}