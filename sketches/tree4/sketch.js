function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);
  seed = 43;
}

let lenMin = 5;
let lenMax = 60;
let lenMul = 0.06;
let wind;

function draw() {
  wind = ((mouseX / width) - 0.5) * 1000
  
  randomSeed(seed)
  background("white");
  push()
  translate(width / 2, height)
  branch(lenMax)
  pop()
}

function branch(len) {
  push()
  if (len > lenMin) {
    strokeWeight(map(len, lenMin, lenMax, 1, 5))
    line(0, 0, 0, -len)
    translate(0, -len)
    push()
    rotate(random(-15, -25) - len * lenMul + (1 / pow(len, 1.3)) * wind)
    branch(len * random(0.65, 0.9))
    pop()
    rotate(random(15, 25) + len * lenMul + (1 / pow(len, 1.3)) * wind)
    branch(len * random(0.65, 0.9))
  } else  if (lenMin < 12) {
    let r = 188 + random(-10, 10)
    let g = 39 + random(-10, 10)
    let b = 57 + random(-10, 10)
    fill(r, g, b)
    noStroke()
    
    beginShape()
    let rad = 15
    for (let i = 45; i < 135; i++) {
      let x = rad * cos(i)
      let y = rad * sin(i)
      vertex(x, y)
    }
    endShape(CLOSE)
  }
  pop()
}

function mouseClicked() {
  seed = random(0, 100)
  redraw()
}
