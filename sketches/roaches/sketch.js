let frame = 0;
let names = [];

function _addName() {
  let x, y;
  let direction = random([1, 2, 3, 4])
  if (direction == 1) {
    x = - width / 10
    y = random(-height / 10, height * 1.1)
  } else if (direction == 2) {
    x = random(-width / 10, width * 1.1)
    y = -height / 10
  } else if (direction == 3) {
    x = width + width / 10
    y = random(-height / 10, height * 1.1)
  } else {
    x = random(-width / 10, width * 1.1)
    y = height + height / 10
  }
  let txt = new Roach("", x, y, 24)
  names.push(txt)
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  frame++;
  if (frame % 80 == 0) {
    _addName()
  }
  background("#F3F3EC");

  for (let i = 0; i<names.length; i++) {
    names[i].display()
  }

}