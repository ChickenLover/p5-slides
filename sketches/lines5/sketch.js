let lines = 6;
let pointsPerLine = 6;

function setup() {
  createCanvas(windowWidth, windowHeight);
  noLoop()
}

function drawLine() {
  let pointsOffset = width / pointsPerLine;
  let points = [];
  for (let i = 1; i < pointsPerLine; i++) {
    let p = {
      x: i * pointsOffset,
      y: 0
    };
    points.push(p);
    push();
    strokeWeight(5);
    point(p.x, p.y);
    pop();
  }

  for (let i = 0; i < points.length - 1; i++) {
    line(points[i].x, points[i].y, points[i+1].x, points[i+1].y);
  }
}

function draw() {
  let lineOffset = height / lines;
  background("white");
  for (let y = 1; y < lines; y++) {
    push()
    translate(0, y * lineOffset)
    drawLine()
    pop()
  }
}