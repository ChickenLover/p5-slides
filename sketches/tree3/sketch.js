function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);
  noLoop()
  seed = 66.07725005596876;
}

let lenMin = 41;
let lenMax = 60;
let lenMul = 0.06;

function draw() {
  randomSeed(seed)
  background("white");
  push()
  translate(width / 2, height)
  branch(lenMax)
  pop()
}

function branch(len) {
  push()
  if (len > lenMin) {
    strokeWeight(map(len, lenMin, lenMax, 1, 5))
    line(0, 0, 0, -len)
    translate(0, -len)
    push()
    rotate(random(-15, -25) - len * lenMul)
    branch(len * random(0.65, 0.9))
    pop()
    rotate(random(15, 25) + len * lenMul)
    branch(len * random(0.65, 0.9))
  } else  if (lenMin < 12) {
    let r = 188 + random(-10, 10)
    let g = 39 + random(-10, 10)
    let b = 57 + random(-10, 10)
    fill(r, g, b)
    noStroke()
    
    beginShape()
    let rad = 15
    for (let i = 45; i < 135; i++) {
      let x = rad * cos(i)
      let y = rad * sin(i)
      vertex(x, y)
    }
    endShape(CLOSE)
  }
  pop()
}

function mouseClicked() {
  lenMin -= 3;
  if (lenMin < 2) {
    lenMin = 41
    seed = random(0, 100)
  }
  redraw()
}
