let tiles = 1

function setup() {
  rectMode(CENTER);
  createCanvas(windowWidth, windowHeight);
  noLoop()
}

function drawShape(w, h) {
  fill("black");
  let r = random();
  if (r < 0.25) {
    rect(w/4, h/4 , w/2, h/2)
    rect(w*3/4, h*3/4 , w/4, h/4)
  } else if (r < 0.5) {
    ellipse(w/2, h/2, w/2, h/2);
  } else if (r < 0.75) {
    triangle(0, 0, w, 0, 0, h);
  } else {
    triangle(w, h, w, 0, 0, h);
  }
}

function draw() {
  background("white");
  let tileWidth = width / tiles;
  let tileHeight = height / tiles;

  for (let x = 0; x < tiles; x++) {
    for (let y = 0; y < tiles; y++) {
      push()
      translate(x * tileWidth, y * tileHeight);
      drawShape(tileWidth, tileHeight)
      pop()
    }
  }
}

function mouseClicked() {
  tiles *= 2
  redraw()
}