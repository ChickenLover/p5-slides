let direction;

function setup() {
  createCanvas(windowWidth, windowHeight);
  rectMode(CENTER);
}

function recDraw(w) {
  if (w <= 5) {
    rect(0, 0, 5, 5);
    return;
  }
  ellipse(0, 0, w, w);

  direction = createVector(mouseX - width / 2, mouseY - height / 2).normalize().setMag(w / 30);
  translate(direction.x, direction.y)
  // Call self
  recDraw(w - 50);
}

function draw() {
  translate(width/2, height/2);
  recDraw(width);
}