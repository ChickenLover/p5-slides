let seed = 42;

function setup() {
  createCanvas(windowWidth, windowHeight);
  rectMode(CENTER);
}

function recDraw(w) {
  if (w < 25) {
    return;
  }
  rect(0, 0, w, w);

  translate(random(-10, 10), random(-10, 10))
  // Call self
  recDraw(w - 50);
}

function draw() {
  randomSeed(seed);

  translate(width/2, height/2);
  recDraw(width);
}

function mouseClicked() {
  seed = random(0, 100);
}