
function drawH1() {
  fill("#bc2739")
  beginShape();
  vertex(48.5, -30.0);
  vertex(88.5, -30.0);
  vertex(76.587, -12.8571);
  vertex(76.587, 12.8571);
  vertex(88.5, 30.0);
  vertex(48.5, 30.0);
  vertex(48.5, -30.0);
  endShape();

  fill("#f3f3ec")
  beginShape();
  vertex(63.5, 9.0);
  bezierVertex(67.366, 9.0,70.5, 11.0147,70.5, 13.5);
  bezierVertex(70.5, 15.9853,67.366, 18.0,63.5, 18.0);
  bezierVertex(59.634, 18.0,56.5, 15.9853,56.5, 13.5);
  bezierVertex(56.5, 11.0147,59.634, 9.0,63.5, 9.0);
  endShape();

  fill("#f3f3ec")
  beginShape();
  vertex(63.5, -18.0);
  bezierVertex(67.366, -18.0,70.5, -15.9853,70.5, -13.5);
  bezierVertex(70.5, -11.0147,67.366, -9.0,63.5, -9.0);
  bezierVertex(59.634, -9.0,56.5, -11.0147,56.5, -13.5);
  bezierVertex(56.5, -15.9853,59.634, -18.0,63.5, -18.0);
  endShape();
}

function drawB1() {
  fill("#bc2739")
  beginShape();
  vertex(-64.5, -36.0);
  vertex(32.5, -36.0);
  bezierVertex(45.755, -36.0,56.5, -25.2548,56.5, -12.0);
  vertex(56.5, 12.0);
  bezierVertex(56.5, 25.2548,45.755, 36.0,32.5, 36.0);
  vertex(-64.5, 36.0);
  vertex(-64.5, -36.0);
  endShape();
}

function drawT1() {
  fill("#bc2739")
  beginShape();
  vertex(-64.5, -36.0);
  vertex(-88.5, -31.0);
  vertex(-74.5435, -21.5);
  vertex(-82.587, 0.0);
  vertex(-74.5435, 21.5);
  vertex(-88.5, 31.0);
  vertex(-64.5, 36.0);
  vertex(-64.5, -36.0);
  endShape();
}

function drawH2() {
  fill("#bc2739")
  beginShape();
  vertex(42.0, -30.0);
  vertex(82.0, -30.0);
  vertex(70.087, -12.8571);
  vertex(70.087, 12.8571);
  vertex(82.0, 30.0);
  vertex(42.0, 30.0);
  vertex(42.0, -30.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(57.0, 9.0);
  bezierVertex(60.866, 9.0,64.0, 11.0147,64.0, 13.5);
  bezierVertex(64.0, 15.9853,60.866, 18.0,57.0, 18.0);
  bezierVertex(53.134, 18.0,50.0, 15.9853,50.0, 13.5);
  bezierVertex(50.0, 11.0147,53.134, 9.0,57.0, 9.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(57.0, -18.0);
  bezierVertex(60.866, -18.0,64.0, -15.9853,64.0, -13.5);
  bezierVertex(64.0, -11.0147,60.866, -9.0,57.0, -9.0);
  bezierVertex(53.134, -9.0,50.0, -11.0147,50.0, -13.5);
  bezierVertex(50.0, -15.9853,53.134, -18.0,57.0, -18.0);
  endShape();
}

function drawB2() {
  fill("#bc2739")
  beginShape();
  vertex(-82.0, -9.3951);
  bezierVertex(-82.0, -23.6246,-69.69, -34.7293,-55.5357, -33.2682);
  vertex(-6.9643, -28.2544);
  bezierVertex(-5.3258, -28.0852,-3.6742, -28.0852,-2.0357, -28.2544);
  vertex(46.536, -33.2682);
  bezierVertex(60.69, -34.7293,73.0, -23.6246,73.0, -9.3951);
  vertex(73.0, 9.3951);
  bezierVertex(73.0, 23.6246,60.69, 34.7293,46.536, 33.2682);
  vertex(-2.0357, 28.2544);
  bezierVertex(-3.6742, 28.0852,-5.3258, 28.0852,-6.9643, 28.2544);
  vertex(-55.5357, 33.2682);
  bezierVertex(-69.69, 34.7293,-82.0, 23.6246,-82.0, 9.3951);
  vertex(-82.0, -9.3951);
  endShape();
}

function drawT2() {
  fill("#bc2739")
  beginShape();
  vertex(-67.0, -30.0);
  vertex(-78.7174, -17.9167);
  vertex(-78.7174, 0.6);
  vertex(-78.7174, 17.9167);
  vertex(-67.0, 30.0);
  vertex(-67.0, -30.0);
  endShape();
}

function drawH3() {
  fill("#bc2739")
  beginShape();
  vertex(39.5, -30.0);
  vertex(79.5, -30.0);
  vertex(67.587, -12.8571);
  vertex(67.587, 12.8571);
  vertex(79.5, 30.0);
  vertex(39.5, 30.0);
  vertex(39.5, -30.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(61.5, 9.0);
  bezierVertex(65.366, 9.0,68.5, 11.0147,68.5, 13.5);
  bezierVertex(68.5, 15.9853,65.366, 18.0,61.5, 18.0);
  bezierVertex(57.634, 18.0,54.5, 15.9853,54.5, 13.5);
  bezierVertex(54.5, 11.0147,57.634, 9.0,61.5, 9.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(61.5, -18.0);
  bezierVertex(65.366, -18.0,68.5, -15.9853,68.5, -13.5);
  bezierVertex(68.5, -11.0147,65.366, -9.0,61.5, -9.0);
  bezierVertex(57.634, -9.0,54.5, -11.0147,54.5, -13.5);
  bezierVertex(54.5, -15.9853,57.634, -18.0,61.5, -18.0);
  endShape();
}

function drawT3() {
  fill("#bc2739")
  beginShape();
  vertex(-55.5, -30.0);
  vertex(-79.5, -25.8333);
  vertex(-65.5435, -17.9167);
  vertex(-65.5435, 0.6);
  vertex(-65.5435, 17.9167);
  vertex(-79.5, 25.8333);
  vertex(-55.5, 30.0);
  vertex(-55.5, -30.0);
  endShape();
}

function drawH4() {
  fill("#bc2739")
  beginShape();
  vertex(57.0, -30.0);
  vertex(97.0, -30.0);
  vertex(85.087, -12.8571);
  vertex(85.087, 12.8571);
  vertex(97.0, 30.0);
  vertex(57.0, 30.0);
  vertex(57.0, -30.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(71.0, 9.0);
  bezierVertex(74.866, 9.0,78.0, 11.0147,78.0, 13.5);
  bezierVertex(78.0, 15.9853,74.866, 18.0,71.0, 18.0);
  bezierVertex(67.134, 18.0,64.0, 15.9853,64.0, 13.5);
  bezierVertex(64.0, 11.0147,67.134, 9.0,71.0, 9.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(71.0, -18.0);
  bezierVertex(74.866, -18.0,78.0, -15.9853,78.0, -13.5);
  bezierVertex(78.0, -11.0147,74.866, -9.0,71.0, -9.0);
  bezierVertex(67.134, -9.0,64.0, -11.0147,64.0, -13.5);
  bezierVertex(64.0, -15.9853,67.134, -18.0,71.0, -18.0);
  endShape();
}

function drawB4() {
  fill("#bc2739")
  beginShape();
  vertex(-79.0, -22.808);
  bezierVertex(-79.0, -26.8978,-75.9153, -30.329,-71.8485, -30.7628);
  vertex(-4.8485, -37.9095);
  bezierVertex(-4.2844, -37.9697,-3.7156, -37.9697,-3.1515, -37.9095);
  vertex(63.849, -30.7628);
  bezierVertex(67.915, -30.329,71.0, -26.8978,71.0, -22.808);
  vertex(71.0, 22.808);
  bezierVertex(71.0, 26.8978,67.915, 30.329,63.849, 30.7628);
  vertex(-3.1515, 37.9095);
  bezierVertex(-3.7156, 37.9697,-4.2844, 37.9697,-4.8485, 37.9095);
  vertex(-71.8485, 30.7628);
  bezierVertex(-75.9153, 30.329,-79.0, 26.8978,-79.0, 22.808);
  vertex(-79.0, -22.808);
  endShape();
}

function drawT4() {
  fill("#bc2739")
  beginShape();
  vertex(-73.0, -30.0);
  vertex(-97.0, -25.8333);
  vertex(-83.0435, -17.9167);
  vertex(-91.087, 0.0);
  vertex(-83.0435, 17.9167);
  vertex(-97.0, 25.8333);
  vertex(-73.0, 30.0);
  vertex(-73.0, -30.0);
  endShape();
}

function drawH5() {
  fill("#bc2739")
  beginShape();
  vertex(37.5, -18.0);
  vertex(77.5, -18.0);
  vertex(65.587, -7.7143);
  vertex(65.587, 7.7143);
  vertex(77.5, 18.0);
  vertex(37.5, 18.0);
  vertex(37.5, -18.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(52.5, 4.0);
  bezierVertex(56.366, 4.0,59.5, 6.0147,59.5, 8.5);
  bezierVertex(59.5, 10.9853,56.366, 13.0,52.5, 13.0);
  bezierVertex(48.634, 13.0,45.5, 10.9853,45.5, 8.5);
  bezierVertex(45.5, 6.0147,48.634, 4.0,52.5, 4.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(52.5, -13.0);
  bezierVertex(56.366, -13.0,59.5, -10.9853,59.5, -8.5);
  bezierVertex(59.5, -6.0147,56.366, -4.0,52.5, -4.0);
  bezierVertex(48.634, -4.0,45.5, -6.0147,45.5, -8.5);
  bezierVertex(45.5, -10.9853,48.634, -13.0,52.5, -13.0);
  endShape();
}

function drawB5() {
  fill("#bc2739")
  beginShape();
  vertex(-59.5, -13.3158);
  bezierVertex(-59.5, -17.7341,-55.9183, -21.3158,-51.5, -21.3158);
  vertex(-31.79, -21.3158);
  bezierVertex(-31.2646, -21.3158,-30.7405, -21.3676,-30.2253, -21.4703);
  vertex(-4.0647, -26.6879);
  bezierVertex(-3.0318, -26.8939,-1.9682, -26.8939,-0.9353, -26.6879);
  vertex(25.225, -21.4703);
  bezierVertex(25.74, -21.3676,26.265, -21.3158,26.79, -21.3158);
  vertex(46.5, -21.3158);
  bezierVertex(50.918, -21.3158,54.5, -17.7341,54.5, -13.3158);
  vertex(54.5, 13.3158);
  bezierVertex(54.5, 17.7341,50.918, 21.3158,46.5, 21.3158);
  vertex(26.79, 21.3158);
  bezierVertex(26.265, 21.3158,25.74, 21.3675,25.225, 21.4703);
  vertex(-0.9353, 26.6879);
  bezierVertex(-1.9682, 26.8939,-3.0318, 26.8939,-4.0647, 26.6879);
  vertex(-30.2253, 21.4703);
  bezierVertex(-30.7405, 21.3675,-31.2646, 21.3158,-31.79, 21.3158);
  vertex(-51.5, 21.3158);
  bezierVertex(-55.9183, 21.3158,-59.5, 17.7341,-59.5, 13.3158);
  vertex(-59.5, -13.3158);
  endShape();
}

function drawT5() {
  fill("#bc2739")
  beginShape();
  vertex(-53.5, -21.0);
  vertex(-77.5, -18.0833);
  vertex(-63.5435, -12.5417);
  vertex(-71.587, 0.0);
  vertex(-63.5435, 12.5417);
  vertex(-77.5, 18.0833);
  vertex(-53.5, 21.0);
  vertex(-53.5, -21.0);
  endShape();
}

function drawH6() {
  fill("#bc2739")
  beginShape();
  vertex(37.5, -18.0);
  vertex(77.5, -26.0);
  vertex(65.587, -7.7143);
  vertex(65.587, 7.7143);
  vertex(77.5, 26.0);
  vertex(37.5, 18.0);
  vertex(37.5, -18.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(52.5, 4.0);
  bezierVertex(56.366, 4.0,59.5, 6.0147,59.5, 8.5);
  bezierVertex(59.5, 10.9853,56.366, 13.0,52.5, 13.0);
  bezierVertex(48.634, 13.0,45.5, 10.9853,45.5, 8.5);
  bezierVertex(45.5, 6.0147,48.634, 4.0,52.5, 4.0);
  endShape();
  fill("#f3f3ec")
  beginShape();
  vertex(52.5, -13.0);
  bezierVertex(56.366, -13.0,59.5, -10.9853,59.5, -8.5);
  bezierVertex(59.5, -6.0147,56.366, -4.0,52.5, -4.0);
  bezierVertex(48.634, -4.0,45.5, -6.0147,45.5, -8.5);
  bezierVertex(45.5, -10.9853,48.634, -13.0,52.5, -13.0);
  endShape();
}

function drawB6() {
  fill("#bc2739")
  beginShape();
  vertex(-59.5, -13.3157);
  bezierVertex(-59.5, -17.734,-55.9183, -21.3157,-51.5, -21.3157);
  vertex(-31.0, -21.3157);
  vertex(-3.1479, -19.0525);
  bezierVertex(-2.7167, -19.0175,-2.2833, -19.0175,-1.8521, -19.0525);
  vertex(26.0, -21.3157);
  vertex(46.5, -21.3157);
  bezierVertex(50.918, -21.3157,54.5, -17.734,54.5, -13.3157);
  vertex(54.5, 13.3159);
  bezierVertex(54.5, 17.7342,50.918, 21.3159,46.5, 21.3159);
  vertex(26.0, 21.3159);
  vertex(-1.8521, 19.0528);
  bezierVertex(-2.2833, 19.0177,-2.7167, 19.0177,-3.1479, 19.0528);
  vertex(-31.0, 21.3159);
  vertex(-51.5, 21.3159);
  bezierVertex(-55.9183, 21.3159,-59.5, 17.7342,-59.5, 13.3159);
  vertex(-59.5, -13.3157);
  endShape();
}

function drawT6() {
  fill("#bc2739")
  beginShape();
  vertex(-53.5, -21.0);
  vertex(-77.5, -18.0833);
  vertex(-63.5435, -12.5417);
  vertex(-63.5435, 0.0);
  vertex(-63.5435, 12.5417);
  vertex(-77.5, 18.0833);
  vertex(-53.5, 21.0);
  vertex(-53.5, -21.0);
  endShape(); 
}

HEADS = [drawH1, drawH2, drawH3, drawH4, drawH5, drawH6]
BODIES = [drawB1, drawB2, drawB1, drawB4, drawB5, drawB6]
TAILS = [drawT1, drawT2, drawT3, drawT4, drawT5, drawT6]

let SPEED = 0.003

class Roach {
  constructor(txt, x, y, fs) {
    this.txt = txt
    this.fs = fs
    this.box = font.textBounds(this.txt, x, y, fs);
    this.x = x
    this.y = y

    let index = int(random(0, 5))
    this.head = HEADS[index]
    this.body = BODIES[index]
    this.tail = TAILS[index]

    //this.head = random(HEADS)
    //this.body = random(BODIES)
    //this.tail = random(TAILS)

    this.t = random(0, 500)
    this.T = random(1000, 1500)
  }

  display() {
    push()
    fill("#BC2739")

    let new_x = map(noise(this.t),0,1,0,width);
    let new_y = map(noise(this.T),0,1,0,height);
    this.t += SPEED;
    this.T += SPEED;

    let v = createVector(new_x - this.x, new_y - this.y).normalize()
    let v2 = v.setMag(20)
    strokeWeight(3)
    line(this.x, this.y, this.x + v2.x, this.y + v2.y)
    let angle = v.heading()
    translate(new_x, new_y)
    rotate(angle)

    noStroke()
    push()
    scale(1.2, 1)
    this.body()
    this.head()
    this.tail()
    pop()

    this.x = new_x
    this.y = new_y
    fill("#F3F3EC")
    textSize(this.fs)
    text(this.txt, 0, this.box.h/2);
    pop()
  }
}
